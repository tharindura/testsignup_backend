/*
 * Copyright (c) 2021. CodeGen International (Pvt) Ltd. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of CodeGen
 * International (Pvt) Ltd. ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with CodeGen International.
 *
 */
package com.example.sociallogin.utils;

import org.springframework.util.SerializationUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.Optional;

public class CookieUtils
{
    public static Optional<Cookie> getCookie( HttpServletRequest request, String name ){
        Cookie[] cookies = request.getCookies();

        if( cookies != null && cookies.length > 0 ) {
            for( Cookie cookie : cookies )
            {
                if( cookie.getName().equals( name ) ){
                    return Optional.of( cookie );
                }
            }
        }
        return Optional.empty();
    }

    public static void addCookie( HttpServletResponse response, String name, String value, int maxAge){
        Cookie cookie = new Cookie( name, value );
        cookie.setPath( "/" );
        cookie.setHttpOnly( true );
        cookie.setMaxAge( maxAge );
        response.addCookie( cookie );
    }

    public static void deleteCookie( HttpServletRequest request, HttpServletResponse response, String name ){
        Cookie[] cookies = request.getCookies();
        if( cookies != null && cookies.length > 0 ){
            for( Cookie cookie : cookies )
            {
                if( cookie.getName().equals( name ) ){
                    cookie.setValue( "" );
                    cookie.setPath( "/" );
                    cookie.setMaxAge( 0 );
                    response.addCookie( cookie );
                }
            }
        }
    }

    public static String serialize(Object object) {
        return Base64.getUrlEncoder()
                     .encodeToString(SerializationUtils.serialize(object));
    }

    public static <T> T deserialize(Cookie cookie, Class<T> cls) {
        return cls.cast( SerializationUtils.deserialize(
                Base64.getUrlDecoder().decode(cookie.getValue())));
    }
}
