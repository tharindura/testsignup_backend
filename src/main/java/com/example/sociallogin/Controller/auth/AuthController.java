/*
 * Copyright (c) 2021. CodeGen International (Pvt) Ltd. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of CodeGen
 * International (Pvt) Ltd. ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with CodeGen International.
 *
 */
package com.example.sociallogin.Controller.auth;

import com.example.sociallogin.exception.BadRequestException;
import com.example.sociallogin.model.AuthProvider;
import com.example.sociallogin.model.User;
import com.example.sociallogin.payload.ApiResponse;
import com.example.sociallogin.payload.AuthResponse;
import com.example.sociallogin.payload.LoginRequest;
import com.example.sociallogin.payload.SignUpRequest;
import com.example.sociallogin.security.oauth2.repository.UserRepository;
import com.example.sociallogin.security.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping( "/auth" )
public class AuthController
{
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenProvider tokenProvider;

    @PostMapping( "/login" )
    public ResponseEntity<?> authenticateUser( @Valid @RequestBody LoginRequest loginRequest )
    {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication( authentication );
        String token = tokenProvider.createToken( authentication );
        return ResponseEntity.ok( new AuthResponse( token ) );
    }

    @PostMapping( "/signup" )
    public ResponseEntity<?> registerUser( @Valid @RequestBody SignUpRequest signUpRequest )
    {
        if( userRepository.existsByEmail( signUpRequest.getEmail() ) )
        {
            throw new BadRequestException( "Email address already in use." );
        }

        User result = userRepository.save( User.builder().name( signUpRequest.getName() )
                                               .email( signUpRequest.getEmail() )
                                               .password( signUpRequest.getPassword() )
                                               .emailVerified( false )
                                               .provider( AuthProvider.local ).build() );
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path( "/user/me" )
                .buildAndExpand( result.getId() ).toUri();

        return ResponseEntity.created( location )
                             .body( new ApiResponse( true, "User registered successfully@" ) );
    }
}
