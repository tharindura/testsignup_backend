/*
 * Copyright (c) 2021. CodeGen International (Pvt) Ltd. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of CodeGen
 * International (Pvt) Ltd. ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with CodeGen International.
 *
 */
package com.example.sociallogin.Controller.auth;

import com.example.sociallogin.exception.ResourceNotFoundException;
import com.example.sociallogin.model.User;
import com.example.sociallogin.security.oauth2.repository.UserRepository;
import com.example.sociallogin.security.CurrentUser;
import com.example.sociallogin.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController
{
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/me")
    @PreAuthorize( "hasRole('USER')" )
    @Cacheable(cacheNames = "users", key = "#userPrincipal.id")
    public User getCurrentUser( @CurrentUser UserPrincipal userPrincipal ){
        return userRepository.findById( userPrincipal.getId() )
                       .orElseThrow(() -> new ResourceNotFoundException( "User", "id", userPrincipal.getId() ) );
    }
}
