/*
 * Copyright (c) 2021. CodeGen International (Pvt) Ltd. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of CodeGen
 * International (Pvt) Ltd. ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with CodeGen International.
 *
 */
package com.example.sociallogin.security.oauth2;

import com.example.sociallogin.exception.OAuth2AuthenticationProcessingException;
import com.example.sociallogin.model.AuthProvider;
import com.example.sociallogin.model.User;
import com.example.sociallogin.security.oauth2.repository.UserRepository;
import com.example.sociallogin.security.UserPrincipal;
import com.example.sociallogin.security.oauth2.user.OAuth2UserInfo;
import com.example.sociallogin.security.oauth2.user.OAuth2UserInfoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService
{
    @Autowired
    private UserRepository userRepository;

    @Override
    public OAuth2User loadUser( OAuth2UserRequest oAuth2UserRequest ) throws OAuth2AuthenticationException
    {
        OAuth2User oAuth2User = super.loadUser( oAuth2UserRequest );

        try
        {
            return processOAuth2User( oAuth2UserRequest, oAuth2User );
        }
        catch( Exception ex )
        {
            throw new InternalAuthenticationServiceException( ex.getMessage(), ex.getCause() );
        }
    }

    private OAuth2User processOAuth2User( OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User )
    {
        OAuth2UserInfo oaUth2UserInfo = OAuth2UserInfoFactory.getOAUth2UserInfo( oAuth2UserRequest.getClientRegistration().getRegistrationId(), oAuth2User.getAttributes() );
        if( StringUtils.isEmpty( oaUth2UserInfo.getEmail() ) )
        {
            throw new OAuth2AuthenticationProcessingException( "Email not found from OAuth2 provider" );
        }

        Optional<User> userOptional = userRepository.findByEmail( oaUth2UserInfo.getEmail() );
        User user;
        if( userOptional.isPresent() )
        {
            user = userOptional.get();
            if( !user.getProvider().equals( AuthProvider.valueOf( oAuth2UserRequest.getClientRegistration().getRegistrationId() ) ) )
            {
                throw new OAuth2AuthenticationProcessingException( "Looks like you're signed up with " + user.getProvider()
                                                                           + " account. Please use your " + user.getProvider()
                                                                           + " account to login." );
            }
            user = updateExistingUser( user, oaUth2UserInfo );
        }
        else
        {
            user = registerNewUser( oAuth2UserRequest, oaUth2UserInfo );
        }
        return UserPrincipal.create( user, oAuth2User.getAttributes() );
    }

    private User registerNewUser( OAuth2UserRequest oAuth2UserRequest, OAuth2UserInfo oAuth2UserInfo )
    {
        return userRepository.save( User.builder()
                                        .provider( AuthProvider.valueOf( oAuth2UserRequest.getClientRegistration().getRegistrationId() ) )
                                        .providerId( oAuth2UserInfo.getId() )
                                        .name( oAuth2UserInfo.getName() )
                                        .email( oAuth2UserInfo.getEmail() )
                                        .imageUrl( oAuth2UserInfo.getImageUrl() )
                                        .emailVerified( false )
                                        .build() );
    }

    private User updateExistingUser( User existingUser, OAuth2UserInfo oAuth2UserInfo )
    {
        existingUser.setName( oAuth2UserInfo.getName() );
        existingUser.setImageUrl( oAuth2UserInfo.getImageUrl() );
        return userRepository.save( existingUser );
    }


}
