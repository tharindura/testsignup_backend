/*
 * Copyright (c) 2021. CodeGen International (Pvt) Ltd. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of CodeGen
 * International (Pvt) Ltd. ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with CodeGen International.
 *
 */
package com.example.sociallogin.security.oauth2.user;

import com.example.sociallogin.exception.OAuth2AuthenticationProcessingException;
import com.example.sociallogin.model.AuthProvider;

import java.util.Map;

public class OAuth2UserInfoFactory
{
    public static OAuth2UserInfo getOAUth2UserInfo( String registrationId, Map<String, Object> attributes ){
        if( registrationId.equalsIgnoreCase( AuthProvider.google.toString() ) ){
            return new GoogleOAuth2UserInfo( attributes );
        } else if( registrationId.equalsIgnoreCase( AuthProvider.facebook.toString() ) ){
            return new FacebookOAuth2UserInfo( attributes );
        }else if( registrationId.equalsIgnoreCase( AuthProvider.github.toString() ) ){
            return new GithubOAuth2UserInfo( attributes );
        }else {
            throw new OAuth2AuthenticationProcessingException( "Sorry! Login with " + registrationId + "is not supported yet." );
        }
    }

}
