/*
 * Copyright (c) 2021. CodeGen International (Pvt) Ltd. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of CodeGen
 * International (Pvt) Ltd. ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with CodeGen International.
 *
 */
package com.example.sociallogin.security.oauth2;

import com.example.sociallogin.utils.CookieUtils;
import nl.altindag.ssl.util.StringUtils;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpCookieOAuth2AuthorizationRequestRepository implements AuthorizationRequestRepository
{
    public static final String OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME = "oauth2_auth_request";
    public static final String REDIRECT_URI_PARAM_COOKIE_NAME = "redirect_uri";
    private static final int cookieExpireSeconds = 180;

    @Override
    public OAuth2AuthorizationRequest loadAuthorizationRequest( HttpServletRequest request )
    {
        return CookieUtils.getCookie(  request, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME )
                          .map( cookie -> CookieUtils.deserialize( cookie, OAuth2AuthorizationRequest.class ) )
                          .orElse( null );
    }

    @Override
    public void saveAuthorizationRequest( OAuth2AuthorizationRequest authorizationRequest, HttpServletRequest request, HttpServletResponse response )
    {
        if( authorizationRequest == null )
        {
            CookieUtils.deleteCookie( request, response, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME );
            CookieUtils.deleteCookie( request, response, REDIRECT_URI_PARAM_COOKIE_NAME );
            return;
        }
        CookieUtils.addCookie( response, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME, CookieUtils.serialize( authorizationRequest ), cookieExpireSeconds );
        String redirectUriAfterLogin = request.getParameter( REDIRECT_URI_PARAM_COOKIE_NAME );

        if( StringUtils.isNotBlank( redirectUriAfterLogin ) )
        {
            CookieUtils.addCookie( response, REDIRECT_URI_PARAM_COOKIE_NAME, redirectUriAfterLogin, cookieExpireSeconds );
        }
    }

    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest( HttpServletRequest request )
    {
        return this.loadAuthorizationRequest( request );
    }

    public void removeAuthorizationRequestCookies( HttpServletRequest request, HttpServletResponse response )
    {
        CookieUtils.deleteCookie( request, response, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME );
        CookieUtils.deleteCookie( request, response, REDIRECT_URI_PARAM_COOKIE_NAME );
    }
}
