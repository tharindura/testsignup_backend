/*
 * Copyright (c) 2021. CodeGen International (Pvt) Ltd. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of CodeGen
 * International (Pvt) Ltd. ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with CodeGen International.
 *
 */
package com.example.sociallogin.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties( prefix = "app" )
public class AppProperties
{
    private final Auth auth = new Auth();
    private final OAuth2 oauth2 = new OAuth2();

    public static class Auth
    {
        private String tokenSecret;
        private long tokenExpirationMsec;

        public String getTokenSecret()
        {
            return tokenSecret;
        }

        public void setTokenSecret( String tokenSecret )
        {
            this.tokenSecret = tokenSecret;
        }

        public long getTokenExpirationMsec()
        {
            return tokenExpirationMsec;
        }

        public void setTokenExpirationMsec( long tokenExpirationMsec )
        {
            this.tokenExpirationMsec = tokenExpirationMsec;
        }
    }

    public static final class OAuth2
    {
        private List<String> authorizedRedirectUris = new ArrayList<>();

        public List<String> getAuthorizedRedirectUris()
        {
            return authorizedRedirectUris;
        }

        public OAuth2 setAuthorizedRedirectUris( List<String> authorizedRedirectUris )
        {
            this.authorizedRedirectUris = authorizedRedirectUris;
            return this;
        }
    }

    public Auth getAuth()
    {
        return auth;
    }

    public OAuth2 getOauth2()
    {
        return oauth2;
    }

}
