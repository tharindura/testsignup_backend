package com.example.sociallogin;

import com.example.sociallogin.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication( scanBasePackages = {
        "com.example.sociallogin" } )
@EnableCaching
@EnableConfigurationProperties( AppProperties.class )
public class SocialLoginApplication
{

    public static void main( String[] args )
    {
        SpringApplication.run( SocialLoginApplication.class, args );
    }

}
