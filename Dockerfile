# Stage 1: Build the JAR
FROM maven:3.8.4-openjdk-17 as build
WORKDIR /app
COPY . /app
RUN mvn package

# Stage 2: Create the final Docker image
FROM openjdk:17
COPY --from=build /app/target/*.jar /app/app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/app.jar"]